"use client";

import React from 'react';
import Link from "next/link";
import {cn} from "@/lib/utils";

interface ButtonProps {
  variant?: 'primary' | 'secondary' | 'ghost' | 'link' | 'destructive' | 'outline';
  size?: 'default' | 'sm' | 'lg' | 'icon';
  asChild?: boolean;
  className?: string;
  href: string;
  target?: string;
  children: React.ReactNode;
  onClick?: () => void;
  type?: 'button' | 'submit' | 'reset';
}
export const Button = ({variant, size, asChild, className, href, target, children, onClick, type}: ButtonProps) => {
  return(
    <Link
      className={cn(
        className,
        "group bg-black text-white text-base lg:text-lg opacity-80 p-2 flex items-center gap-2 rounded-lg outline-none dark:bg-white dark:text-black transition",
      )}
      href={href}
      onClick={onClick}
      target={target}
      type={type}
    >
      {children}
    </Link>
  )
}