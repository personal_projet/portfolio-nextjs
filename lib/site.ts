export const siteConfig = {
  description: "My portfolio website",
  baseUrl: process.env.NEXT_PUBLIC_BASE_URL || "http://localhost:3000",
  linkedin: "https://www.linkedin.com/in/mathis-fumel/",
  github: "https://gitlab.com/mfumel",
}