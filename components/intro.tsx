"use client";

import { motion } from "framer-motion";
import Image from "next/image";
import { FaGitlab, FaLinkedinIn } from "react-icons/fa";

import { Button } from "@/components/button";
import { useActiveSectionContext } from "@/context/active-section-context";
import { useSectionInView } from "@/lib/hooks";
import { siteConfig } from "@/lib/site";
import { BsArrowRight } from "react-icons/bs";

export default function Intro() {
  const { ref } = useSectionInView("Home", 0.5);
  const { setActiveSection, setTimeOfLastClick } = useActiveSectionContext();

  return (
    <section
      ref={ref}
      id="home"
      className="h-[80vh] lg:h-[60vh] my-8 scroll-mt-[100rem] flex flex-col items-center justify-center gap-10 lg:flex-row-reverse"
    >
      <div className="flex items-center justify-center">
        <div className="relative">
          <motion.div
            initial={{ opacity: 0, scale: 0 }}
            animate={{ opacity: 1, scale: 1 }}
            transition={{
              type: "tween",
              duration: 0.2,
            }}
          >
            <Image
              src="/me.png"
              alt="Portfolio de Mathis Fumel"
              width="250"
              height="250"
              quality="95"
              priority={true}
              className="h-32 w-32 lg:h-64 lg:w-64 rounded-full object-cover border-[0.35rem] border-white shadow-xl"
            />
          </motion.div>
        </div>
      </div>
      <div className="lg:text-left">
        <motion.h1
          className="mb-10 mt-4 px-4 text-2xl font-medium !leading-[1.5] sm:text-4xl"
          initial={{ opacity: 0, y: 100 }}
          animate={{ opacity: 1, y: 0 }}
        >
          <span className="lg:text-5xl">
            Moi, c'est <span className="font-bold">Mathis Fumel</span> 👋
            <br />
          </span>
          Développeur <span className="font-bold">Typescript, React</span>
          <br />
          <span className="font-bold">5 ans</span> d'expérience dans ce domaine
        </motion.h1>

        <motion.div
          className="flex flex-row flex-wrap items-center justify-center lg:justify-start gap-2 px-4"
          initial={{ opacity: 0, y: 100 }}
          animate={{ opacity: 1, y: 0 }}
          transition={{
            delay: 0.1,
          }}
        >
          <Button href="mailto:fml.mathis@gmail.com" target="_blank">
            fml.mathis@gmail.com
          </Button>

          <Button href={siteConfig.linkedin} target="_blank" className="lg:p-3">
            <FaLinkedinIn />
          </Button>

          <Button href={siteConfig.github} target="_blank" className="lg:p-3">
            <FaGitlab />
          </Button>
          <Button
            href="https://cal.com/mathis-fumel/freelance"
            onClick={() => {
              setActiveSection("Contact");
              setTimeOfLastClick(Date.now());
            }}
          >
            Un ☕️{" "}
            <BsArrowRight className="opacity-70 group-hover:translate-x-1 transition" />
          </Button>
        </motion.div>
      </div>
    </section>
  );
}
