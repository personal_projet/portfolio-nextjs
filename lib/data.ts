import kanbanoImg from "@/public/kanbano.png";
import kiwiid from "@/public/kiwiid.webp";
import nextStream from "@/public/next-stream.png";
import React from "react";
import { CgWorkAlt } from "react-icons/cg";
import { FaExternalLinkAlt, FaGitlab, FaReact } from "react-icons/fa";
import { LuGraduationCap } from "react-icons/lu";


import lpImg from "@/public/lp.png";


export const links = [
  {
    name: "Home",
    hash: "#home",
  },
  {
    name: "Projects",
    hash: "#projects",
  },
  {
    name: "Skills",
    hash: "#skills",
  },
  {
    name: "Experience",
    hash: "#experience",
  },
  {
    name: "Contact",
    hash: "#contact",
  },
] as const;

export const experiencesData = [
  {
    title: "Créateur de Kiwiid",
    location: "Remote",
    description:
      "Kiwiid est un SaaS qui permet de créer un Blog optimisé pour le SEO avec Notion. +150 utilisateurs",
    icon: React.createElement(FaReact),
    date: "2024 - now",
  },
  {
    title: "Fondateur Hello Https",
    location: "Rennes",
    description:
      "J'accompagne les entreprises dans leur transformation digitale éco-responsable.",
    icon: React.createElement(FaReact),
    date: "2022 - now",
  },
  {
    title: "Co-fondateur à GreenToFash",
    location: "Rennes",
    description:
      "Développement d'une application mobile dans le domaine de la mode durable. Premier styliste virtuel pour économiser et être stylé.",
    icon: React.createElement(FaReact),
    date: "2023 - now",
  },
  {
    title: "Développeur Full-Stack",
    location: "Cartelmatic",
    description:
      "Pôle R&D, j'ai travaillé sur des projets de développement web et mobile essentiallement en React, NodeJs.",
    icon: React.createElement(FaReact),
    date: "2022 - 2023",
  },
  {
    title: "Freelance SEO & Web WordPress",
    location: "Rennes",
    description:
      "Durant mes études, j'ai travaillé en tant que freelance SEO. J'ai aidé des entreprises à améliorer leur référencement naturel.",
    icon: React.createElement(CgWorkAlt),
    date: "2018 - 2021",
  },
  {
    title: "Formation Marketing Digitale",
    location: "",
    description:
      "Stratégie de développement d’entreprise",
    icon: React.createElement(LuGraduationCap),
    date: "2023",
  },
  {
    title: "Bachelor Informatique",
    location: "SDV",
    description:
      "Spécialisation Développeur Full-Stack en méthode agile",
    icon: React.createElement(LuGraduationCap),
    date: "2022 - 2023",
  },
  {
    title: "DUT Informatique",
    location: "La Rochelle",
    description:
      "Spécialisation Développement Web.",
    icon: React.createElement(LuGraduationCap),
    date: "2020 - 2022",
  },
] as const;

export const projectsData = [
  {
    title: "Kiwiid",
    description:
      "Kiwiid est un SaaS qui permet de créer un Blog optimisé pour le SEO avec Notion. +150 utilisateurs",
    tags: ["Next.js", "Prisma", "Stripe"],
    imageUrl: kiwiid,
    links: [
      {
        icon: React.createElement(FaExternalLinkAlt),
        link: "https://kiwiid.co",
      },
    ],
  },
  {
    title: "Kanbano (Trelo clone)",
    description:
      "Un clone de Trello avec NextJs et Prisma. Il a des fonctionnalités comme le glisser-déposer, le tri, la pagination et la recherche. Essaye le!",
    tags: ["Next.js", "Prisma", "Stripe"],
    imageUrl: kanbanoImg,
    links: [
      {
        icon: React.createElement(FaGitlab),
        link: "https://gitlab.com/personal_projet/trello-clone",
      },
      {
        icon: React.createElement(FaExternalLinkAlt),
        link: "https://kanbano.mathis-fumel.fr",
      },
    ],
  },
  {
    title: "Next Stream (Twitch clone)",
    description:
      "Un clone de Twitch avec NextJs et Prisma. On peux lancer un stream avec une connexion RTMP",
    tags: ["Next.js", "Prisma", "Stripe", "LivKit", "Clerk"],
    imageUrl: nextStream,
    links: [
      {
        icon: React.createElement(FaGitlab),
        link: "https://gitlab.com/personal_projet/twitch-clone",
      },
      {
        icon: React.createElement(FaExternalLinkAlt),
        link: "https://next-stream.mathis-fumel.fr/",
      },
    ],
  },
  {
    title: "Landing page",
    description:
      "Je me suis spécialisé dans la création de landing page éco-responsable en Next.js qui a comme objectif de convertir.",
    tags: ["Next.js", "Prisma", "Docker"],
    imageUrl: lpImg,
    links: [
      {
        icon: React.createElement(FaExternalLinkAlt),
        link: "https://hello-https.fr",
      },
    ],
  },
] as const;

export const skillsData = [
  "NextJs",
  "Docker",
  "TypeScript",
  "React",
  "React Native",
  "Adonisjs",
  "SQL",
  "SEO",
  "Make",
  "FlutterFlow",
  "WordPress",
  "LLM (IA)",
  "AWS",
  "Symfony",
  "Vue",
  "Php8",
  "VPS",
  "CI/CD",
  "NoSql",
  "Prisma",
  "Remix"
] as const;
