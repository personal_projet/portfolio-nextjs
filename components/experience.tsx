"use client";

import React from "react";
import SectionHeading from "./section-heading";
import {experiencesData} from "@/lib/data";
import {useSectionInView} from "@/lib/hooks";
import {useTheme} from "@/context/theme-context";
import { motion } from "framer-motion";


export default function Experience() {
  const fadeInAnimationVariants = {
    initial: {
      opacity: 0,
      y: 100,
    },
    animate: (index: number) => ({
      opacity: 1,
      y: 0,
      transition: {
        delay: 0.05 * index,
      },
    }),
  };

  const { ref } = useSectionInView("Experience");
  const { theme } = useTheme();

  return (
    <section id="experience" ref={ref} className="scroll-mt-28 mb-28 sm:mb-40">
      <SectionHeading>Mon experience</SectionHeading>
      {experiencesData.map((experience, index) => (
        <motion.div
          key={index}
          initial="initial"
          whileInView="animate"
          variants={fadeInAnimationVariants}
          viewport={{
            once: true,
          }}
          className="flex flex-col md:flex-row items-center justify-between w-full"
          custom={index}
        >
          <div className="flex-1 p-4">
            <h3 className="text-lg font-semibold">{experience.title}</h3>
            <p className="text-sm text-gray-500">{experience.location}</p>
            <p className="text-sm">{experience.description}</p>
          </div>
          <span className="h-12 w-12 bg-black text-white text-xl rounded-full shadow-lg flex items-center justify-center mx-4 dark:bg-white dark:text-black">
        {experience.icon}
      </span>
          <div className="flex-1 p-4 text-right">
            <p className="text-sm text-gray-500">{experience.date}</p>
          </div>
        </motion.div>
      ))}

    </section>
  );
}
