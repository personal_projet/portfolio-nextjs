import type { Metadata } from 'next';
import {siteConfig} from "@/lib/site";

interface SeoProps {
  title: string;
  description?: string;
}

export function seo({ title, description }: SeoProps): Metadata {
  // Définissez l'URL de base pour les métadonnées (par exemple, l'URL de votre site)
  const metadataBase = new URL(siteConfig.baseUrl);

  return {
    title: `${title}`,
    description: description || siteConfig.description,
    metadataBase, // Ajoutez metadataBase ici
    openGraph: {
      title,
      description: description || siteConfig.description,
      url: metadataBase.href
      // Si vous utilisez des images, assurez-vous qu'elles ont des URLs absolus ou configurez-les ici
    },
    icons: [
      {
        url: "/logo.png",
        href: "/logo.png",
      },
    ],
    // Vous pouvez également ajouter d'autres champs de métadonnées si nécessaire
  };
}
